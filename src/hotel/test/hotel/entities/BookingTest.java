package hotel.entities;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import hotel.credit.CreditCard;

@ExtendWith(MockitoExtension.class)
class BookingTest {
	@Mock Guest guest;
	@Mock Room room;	
	@Mock CreditCard card;
	List adding;
	ServiceType serviceType;
	SimpleDateFormat format=new SimpleDateFormat();
	
	Booking booking;
	@BeforeEach
	void setUp() throws Exception {		
		format=new SimpleDateFormat("dd-MM-yyyy");
		Date arrivalDate=format.parse("11-12-2001");
		int stayLength=2;
		int numberOfOccupants=5;		
		
		booking=new Booking(guest,room,arrivalDate,stayLength,numberOfOccupants,card);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	/*
	 * Author: Rabin Dulal
	 */
	@Test
	final void testaddServiceCharge() {
		//arrange
		//act		
		booking.addServiceCharge(serviceType, 100.20);
		adding=booking.getCharges();
		ServiceCharge Ser=(ServiceCharge) adding.get(0);
		assertEquals(Ser.getCost(),100.2);
			
	}
	
}
