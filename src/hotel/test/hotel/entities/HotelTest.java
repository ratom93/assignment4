package hotel.entities;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import hotel.checkin.CheckinCTL;
import hotel.credit.CreditCard;
import hotel.credit.CreditCardType;
import hotel.entities.*;
	


public class HotelTest {
	@Mock Hotel hotel;
	@Mock Guest guest;	
	@Mock CreditCard card;
	@Mock Booking confirmationNumber;
	SimpleDateFormat format=new SimpleDateFormat("11-12-2001");
	Date arrivalDate;
	int stayLength=2;
	ServiceType serviceType;
	int numberOfOccupants=5;
	Booking booking;
	public Map<Long, Booking> bookingsByConfirmationNumber;
	public Map<Integer, Booking> activeBookingsByRoomId;
	enum State {OCCUPIED,READY};
	List adding;
	State state;
	@Mock Booking mockBooking;
	@InjectMocks Room room=new Room(101,RoomType.SINGLE);
	@BeforeEach

	void setUp() throws Exception {
		arrivalDate=format.parse("11-12-2001");	
		bookingsByConfirmationNumber = new HashMap<>();
		activeBookingsByRoomId= new HashMap<>();
		hotel=new Hotel();
		booking =new Booking(guest,room,arrivalDate,stayLength,numberOfOccupants,card);

	}
		
	/*
	 * Author: Rabin Dulal
	 	     
	 */
	@Test
	public final void testCheckout() { 
		hotel.book(room,guest,arrivalDate,stayLength,numberOfOccupants,card);
		hotel.checkin(101970101);
		hotel.checkout(101);
		Executable e = () -> hotel.addServiceCharge(101,serviceType, 200);
		Throwable t = assertThrows(RuntimeException.class,e);
		//assert
		assertEquals("Error : There are No Active Bookings Please Verify",t.getMessage());		
		
		
	}
	
}
